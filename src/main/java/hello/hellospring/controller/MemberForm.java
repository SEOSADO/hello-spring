package hello.hellospring.controller;

public class MemberForm {
    // html tag name과 동일하게 맞추면 스프링 내부에서 아래 setName을 통해 값을 자동으로 넣어준다....
    private String name;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }
}
