package hello.hellospring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

// controller 어노테이션 적어줘야함.
@Controller
public class HelloController {
        // hello를 들어가면 여기가 호출된다.
        @GetMapping("hello")
        public String hello(Model model){
            model.addAttribute("data","hello!!dd");
            return "hello";
        }

        @GetMapping("hello-mvc")
        public String HelloMvc(@RequestParam("name") String name, Model model){
            model.addAttribute("name", name);
            return "hello-template";
        }

        @GetMapping("hello-string")
        @ResponseBody //http의 body의 데이터를 직접 넣어주겠다.
        public String HelloString(@RequestParam("name") String name){
            return "hello" + name;
        }

        @GetMapping("hello-api")
        @ResponseBody
        public Hello helloApi(@RequestParam("name") String name){
            Hello hello = new Hello();
            hello.setName(name);
            return hello;
        }

        static class Hello {
            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

}
